package korshunov.factory.components;

abstract public class Component {
    private String identificator;

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }
}
