package korshunov.factory.components;

public class Car extends Component {

    private Engine engine;
    private Accessory accessory;
    private Carcase carcase;

    public Car(String identificator, Engine engine, Carcase carcase, Accessory accessory) {
        setIdentificator(identificator);
        this.engine = engine;
        this.carcase = carcase;
        this.accessory = accessory;
    }

    public Engine getEngine() {
        return engine;
    }

    public Accessory getAccessory() {
        return accessory;
    }

    public Carcase getCarcase() {
        return carcase;
    }
}
