package korshunov.factory;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Vector;

public class Storage<Type> {

    private int capacity;
    private StringProperty sizeProperty = new SimpleStringProperty("0");
    private Vector<Type> storage = new Vector<>(10);

    public Storage(int capacity) {
        this.capacity = capacity;
    }

    public synchronized boolean storeComponent(Type component) {
        if (storage.size() < capacity) {
            int size = storage.size();
            storage.add(component);
            Platform.runLater(() -> sizeProperty.setValue(String.valueOf(storage.size())));
                if (size == 0) {
                    notifyAll();
                }
            return true;
        } else {
            return false;
        }
    }

    public synchronized Type takeComponent() {
        if (storage.size() == 0) {
            return null;
        } else {
            Type component = storage.get(storage.size()-1);
            storage.remove(component);
            Platform.runLater(() -> sizeProperty.setValue(String.valueOf(storage.size())));
            notifyAll();
            return component;
        }
    }

    public boolean isFull() {
        return storage.size() == capacity;
    }

    public boolean isEmpty() {
        return storage.size() == 0;
    }

    public int getCapacity() {
        return capacity;
    }

    public StringProperty getSizeProperty() {
        return sizeProperty;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
