package korshunov.factory.controller;

import korshunov.factory.Storage;
import korshunov.factory.components.Car;
import korshunov.threadpool.Worker;

public class CarStorageController extends Thread {

    private Worker worker;
    private final Storage<Car> storage;

    public CarStorageController(Worker worker, Storage<Car> storage) {
        this.worker = worker;
        this.storage = storage;
    }

    @Override
    public void run() {
        super.run();
        worker.requestCar(storage.getCapacity());
        while (true) {
            try {
                synchronized (storage) {
                    storage.wait();
                    if (!storage.isFull()) {
                        worker.requestCar(1);
                    }
                }
            } catch (InterruptedException e) {
               return;
            }
        }
    }
}
