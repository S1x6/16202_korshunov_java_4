package korshunov.factory;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import korshunov.factory.components.Car;

import java.util.Vector;

public class DealersController {

    private DoubleProperty sleepTime = new SimpleDoubleProperty(5.0);
    private IntegerProperty madeCars = new SimpleIntegerProperty(0);
    private Vector<Dealer> threads = new Vector<>();
    private int num;

    public DealersController(Storage<Car> storage, int num, boolean log) {
        this.num = num;
        for (int i = 1; i <= num; i++) {
            threads.add(new Dealer(i, storage, sleepTime, log));
            threads.get(i-1).setActionListener(e -> this.madeCars.set(madeCars.intValue() + 1));
        }
    }

    public void stop() {
        for (int i = 0; i < this.num; ++i) {
            threads.get(i).interrupt();
        }
    }

    public DoubleProperty getTimeProperty() {
        return sleepTime;
    }

    public IntegerProperty getDealedCarsNumProperty() {
        return madeCars;
    }

    public void start() {
        for (int i = 0; i < num; i++) {
            threads.get(i).start();
        }
    }

}
