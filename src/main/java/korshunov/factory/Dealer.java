package korshunov.factory;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import korshunov.factory.components.Car;

import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Dealer extends Thread {

    private int id;
    private final Storage<Car> storage;
    private ActionListener listener;
    private DoubleProperty sleepTime;
    private boolean log;

    public Dealer(int id, Storage<Car> storage, DoubleProperty property, boolean log) {
        this.storage = storage;
        this.id = id;
        this.sleepTime = property;
        this.log = log;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            try {
                if (isInterrupted())
                    return;
                sleep(sleepTime.intValue() * 1000);
                Car car = storage.takeComponent();

                if (car == null) {
                    synchronized (storage) {
                        while (storage.isEmpty()) {
                            storage.wait();
                        }
                        car = storage.takeComponent();
                    }
                }
                if (log) {
                    logCar(car);
                }
                Platform.runLater(() -> listener.actionPerformed(null));

            } catch (InterruptedException e) {
               return;
            }
        }
    }

    void setActionListener(ActionListener listener) {
        this.listener = listener;
    }


    private void logCar(Car car) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println(sdf.format(cal.getTime()) + ": Dealer " + id + ": Auto " + car.getIdentificator() +
                "(Carcase: " + car.getCarcase().getIdentificator() + ". Engine: " + car.getEngine().getIdentificator() +
                ", Accessory: " + car.getAccessory().getIdentificator() + ")");
    }
}
