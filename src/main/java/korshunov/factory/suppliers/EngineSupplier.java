package korshunov.factory.suppliers;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import korshunov.factory.Storage;
import korshunov.factory.components.Engine;

import java.util.Vector;

public class EngineSupplier implements Runnable {

    private static final String suffix = "E";
    private DoubleProperty assemblyTime = new SimpleDoubleProperty(3.0);
    private int counter;
    private final Storage<Engine> storage;
    private Vector<Thread> threads = new Vector<>();
    private int num;

    public EngineSupplier(Storage<Engine> storage, int num) {
        this.storage = storage;
        this.num = num;
    }

    public void start() {
        for (int i = 0; i < this.num; ++i) {
            threads.add(new Thread(this));
            threads.get(i).start();
        }
    }

    public void stop() {
        for (int i = 0; i < this.num; ++i) {
            threads.get(i).interrupt();
        }
    }

    @Override
    public void run() {
        Engine engine;
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            try {
                Thread.sleep(assemblyTime.getValue().intValue() * 1000);
            } catch (InterruptedException e) {
                return;
            }
            engine = new Engine((counter++) + suffix);
            if (storage.storeComponent(engine)) {
                System.out.println(engine.getIdentificator() + " was stored");
            } else {
                try {
                    synchronized (storage) {
                        while (storage.isFull()) {
                            storage.wait();
                        }
                        storage.storeComponent(engine);
                        System.out.println(engine.getIdentificator() + " was stored after waiting");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public DoubleProperty getAssemblyTimeProperty() {
        return assemblyTime;
    }
}
