package korshunov.factory.suppliers;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import korshunov.factory.Storage;
import korshunov.factory.components.Accessory;

import java.util.Vector;

public class AccessorySupplier implements Runnable {

    private static final String suffix = "A";
    private DoubleProperty assemblyTime = new SimpleDoubleProperty(3.0);
    private int counter;
    private final Storage<Accessory> storage;
    private Vector<Thread> threads = new Vector<>();
    private int num;

    public AccessorySupplier(Storage<Accessory> storage, int num) {
        this.storage = storage;
        this.num = num;
    }

    public void start() {
        for (int i = 0; i < this.num; ++i) {
            threads.add(new Thread(this));
            threads.get(i).start();
        }
    }

    public void stop() {
        for (int i = 0; i < this.num; ++i) {
            threads.get(i).interrupt();
        }
    }

    @Override
    public void run() {
        Accessory accessory;
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            try {

                Thread.sleep(assemblyTime.getValue().intValue() * 1000);
            } catch (InterruptedException e) {
               return;
            }
            accessory = new Accessory((counter++) + suffix);
            if (storage.storeComponent(accessory)) {
                System.out.println(accessory.getIdentificator() + " was stored");
            } else {
                try {
                    synchronized (storage) {
                        while (storage.isFull()) {
                            storage.wait();
                        }
                        storage.storeComponent(accessory);
                        System.out.println(accessory.getIdentificator() + " was stored after waiting");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public DoubleProperty getAssemblyTimeProperty() {
        return assemblyTime;
    }
}
