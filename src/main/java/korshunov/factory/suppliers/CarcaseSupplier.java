package korshunov.factory.suppliers;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import korshunov.factory.Storage;
import korshunov.factory.components.Carcase;

import java.util.Vector;

public class CarcaseSupplier implements Runnable {

    private static final String suffix = "C";
    private DoubleProperty assemblyTime = new SimpleDoubleProperty(3.0);
    private int counter;
    private final Storage<Carcase> storage;
    private Vector<Thread> threads = new Vector<>();
    private int num;

    public CarcaseSupplier(Storage<Carcase> storage, int num) {
        this.storage = storage;
        this.num = num;
    }

    public void start() {
        for (int i = 0; i < this.num; ++i) {
            threads.add(new Thread(this));
            threads.get(i).start();
        }
    }

    public void stop() {
        for (int i = 0; i < this.num; ++i) {
            threads.get(i).interrupt();
        }
    }

    @Override
    public void run() {
        Carcase carcase;
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            try {
                Thread.sleep(assemblyTime.getValue().intValue() * 1000);
            } catch (InterruptedException e) {
               return;
            }
            carcase = new Carcase((counter++) + suffix);
            if (storage.storeComponent(carcase)) {
                System.out.println(carcase.getIdentificator() + " was stored");
            } else {
                try {
                    synchronized (storage) {
                        while (storage.isFull()) {
                            storage.wait();
                        }
                        storage.storeComponent(carcase);
                        System.out.println(carcase.getIdentificator() + " was stored after waitnig");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public DoubleProperty getAssemblyTimeProperty() {
        return assemblyTime;
    }

}
