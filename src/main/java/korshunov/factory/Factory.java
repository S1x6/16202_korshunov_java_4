package korshunov.factory;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import korshunov.factory.components.Accessory;
import korshunov.factory.components.Car;
import korshunov.factory.components.Carcase;
import korshunov.factory.components.Engine;
import korshunov.factory.controller.CarStorageController;
import korshunov.factory.suppliers.AccessorySupplier;
import korshunov.factory.suppliers.CarcaseSupplier;
import korshunov.factory.suppliers.EngineSupplier;
import korshunov.threadpool.Worker;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Factory {

    private AccessorySupplier accessorySupplier;
    private EngineSupplier engineSupplier;
    private CarcaseSupplier carcaseSupplier;
    private Storage<Engine> engineStorage;
    private Storage<Carcase> carcaseStorage;
    private Storage<Car> carStorage;
    private Storage<Accessory> accessoryStorage;
    private CarStorageController storageController;
    private Worker worker;
    private DealersController dealers;

    public Factory(String configFileName) {
        Configs configs = parseConfig(configFileName);
        if (configs == null) {
            configs = new Configs();
        }
        accessoryStorage = new Storage<>(configs.accessoryStorageSize);
        engineStorage = new Storage<>(configs.engineStorageSize);
        carcaseStorage = new Storage<>(configs.carcaseStorageSize);
        carStorage = new Storage<>(configs.carStorageSize);

        accessorySupplier = new AccessorySupplier(accessoryStorage, configs.accessorySuppliers);

        engineSupplier = new EngineSupplier(engineStorage, configs.engineSuppliers);
        carcaseSupplier = new CarcaseSupplier(carcaseStorage,configs.carcaseSuppliers);

        worker = new Worker(accessoryStorage, carcaseStorage, engineStorage, carStorage, 3);
        storageController = new CarStorageController(worker, carStorage);

        dealers = new DealersController(carStorage,configs.dealerSize, configs.log);
    }

    public void start() {
        accessorySupplier.start();
        engineSupplier.start();
        carcaseSupplier.start();
        storageController.start();
        dealers.start();
    }

    public void stop() {
        accessorySupplier.stop();
        engineSupplier.stop();
        carcaseSupplier.stop();
        storageController.interrupt();
        dealers.stop();
    }

    private Configs parseConfig(String fileName) {
        Configs configs = new Configs();
        Scanner scanner;
        try {
            scanner = new Scanner(new File(getClass().getResource(fileName).getFile()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        int check = 0;
        while (scanner.hasNextLine()) {
            String string = scanner.nextLine();
            String[] prop = string.split("=");
            switch (prop[0]) {
                case "StorageBodySize":
                    check += 1;
                    configs.carcaseStorageSize = Integer.parseInt(prop[1]);
                    break;
                case "StorageMotorSize":
                    check += 2;
                    configs.engineStorageSize = Integer.parseInt(prop[1]);
                    break;
                case "StorageAccessorySize":
                    check += 4;
                    configs.accessoryStorageSize = Integer.parseInt(prop[1]);
                    break;
                case "StorageAutoSize":
                    check += 8;
                    configs.carStorageSize = Integer.parseInt(prop[1]);
                    break;
                case "AccessorySuppliers":
                    check += 16;
                    configs.accessorySuppliers = Integer.parseInt(prop[1]);
                    break;
                case "BodySuppliers":
                    check += 32;
                    configs.carcaseSuppliers = Integer.parseInt(prop[1]);
                    break;
                case "EngineSuppliers":
                    check += 64;
                    configs.engineSuppliers = Integer.parseInt(prop[1]);
                    break;
                case "Workers":
                    check += 128;
                    configs.workerSize = Integer.parseInt(prop[1]);
                    break;
                case "Dealers":
                    check += 256;
                    configs.dealerSize = Integer.parseInt(prop[1]);
                    break;
                case "LogSale":
                    check += 512;
                    configs.log = prop[1].equals("true");
                    break;
            }
        }
        if (check != 1023) {
            configs = null;
        }
        return configs;

    }

    public StringProperty getEngineStorageSizeProperty() {
        return engineStorage.getSizeProperty();
    }

    public StringProperty getCarcaseStorageSizeProperty() {
        return carcaseStorage.getSizeProperty();
    }

    public StringProperty getAccessoryStorageSizeProperty() {
        return accessoryStorage.getSizeProperty();
    }

    public StringProperty getCarStorageSizeProperty() {
        return carStorage.getSizeProperty();
    }

    public DoubleProperty getEngineAssemblyTimeProperty() {
        return engineSupplier.getAssemblyTimeProperty();
    }

    public DoubleProperty getCarcaseAssemblyTimeProperty() {
        return carcaseSupplier.getAssemblyTimeProperty();
    }

    public DoubleProperty getAccessoryAssemblyTimeProperty() {
        return accessorySupplier.getAssemblyTimeProperty();
    }

    public DoubleProperty getDealerTimeProperty() {
        return dealers.getTimeProperty();
    }

    public IntegerProperty getDealedCars() {
        return dealers.getDealedCarsNumProperty();
    }

    private class Configs {
        int dealerSize=1, workerSize=1, carStorageSize=10, accessoryStorageSize=10, carcaseStorageSize=10,
                engineStorageSize=10, accessorySuppliers=1, engineSuppliers=1, carcaseSuppliers=1;
        boolean log=true;
    }
}
