package korshunov.threadpool;

import korshunov.factory.components.Accessory;
import korshunov.factory.Storage;
import korshunov.factory.components.Car;
import korshunov.factory.components.Carcase;
import korshunov.factory.components.Engine;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Worker implements Runnable {

    private final Storage<Accessory> accessoryStorage;
    private final Storage<Engine> engineStorage;
    private final Storage<Car> carStorage;
    private final Storage<Carcase> carcaseStorage;
    private final ExecutorService executorService;

    private int id = 0;

    public Worker(Storage<Accessory> accessoryStorage, Storage<Carcase> carcaseStorage, Storage<Engine> engineStorage, Storage<Car> carStorage, int nThreads) {
        this.accessoryStorage = accessoryStorage;
        this.carcaseStorage = carcaseStorage;
        this.engineStorage = engineStorage;
        this.carStorage = carStorage;
        executorService= Executors.newFixedThreadPool(nThreads);
    }

    public void requestCar(int nCars) {
        for (int i = 0; i < nCars; ++i) {
            System.out.println("request car");
            executorService.submit(this);
        }
    }

    @Override
    public void run() {
            Accessory accessory = accessoryStorage.takeComponent();
            if (accessory == null) {
                try {
                    System.out.println("Waiting for accessory");
                    synchronized (accessoryStorage) {
                        accessoryStorage.wait();
                    }
                    System.out.println("Accessory is available");
                    accessory = accessoryStorage.takeComponent();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Accessory is taken");
            Engine engine = engineStorage.takeComponent();
            if (engine == null) {
                try {
                    System.out.println("Waiting for engine");
                    synchronized (engineStorage) {
                        engineStorage.wait();
                    }
                    System.out.println("Engine is available");
                    engine = engineStorage.takeComponent();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Engine is taken");
            Carcase carcase = carcaseStorage.takeComponent();
            if (carcase == null) {
                try {
                    System.out.println("Waiting for carcase");
                    synchronized (carcaseStorage) {
                        carcaseStorage.wait();
                    }
                    System.out.println("Carcase is available");
                    carcase = carcaseStorage.takeComponent();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
             System.out.println("Carcase is taken");
            if (accessory != null && engine != null && carcase != null) {
                Car car = new Car(String.valueOf(id++), engine, carcase, accessory);
                carStorage.storeComponent(car);
            }

        }
}
