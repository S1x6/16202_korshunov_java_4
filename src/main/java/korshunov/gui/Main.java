package korshunov.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static javafx.application.Platform.exit;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Factory Simulator");
        primaryStage.setScene(new Scene(root, 700, 300));
        primaryStage.show();
        Controller controller = loader.getController();
        primaryStage.setOnCloseRequest(event -> {
            controller.stop();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }

}
