package korshunov.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import korshunov.factory.Factory;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    Label lbEngineStorageFullness, lbCarcaseStorageFullness, lbAccessoryStorageFullness,
        lbCarStorageFullness, lbEngineTime, lbCarcaseTime, lbAccessoryTime, lbDealerTime,
        dealerCars;
    @FXML
    Slider slEngineTime, slCarcaseTime, slAccessoryTime, slDealerTime;

    Factory factory;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        factory = new Factory("/config.ini");
        factory.start();
        slEngineTime.valueProperty().addListener((obs, oldVal, newVal) ->
                slEngineTime.setValue(Math.round(newVal.doubleValue())));
        slCarcaseTime.valueProperty().addListener((obs, oldVal, newVal) ->
                slCarcaseTime.setValue(Math.round(newVal.doubleValue())));
        slAccessoryTime.valueProperty().addListener((obs, oldVal, newVal) ->
                slAccessoryTime.setValue(Math.round(newVal.doubleValue())));
        slDealerTime.valueProperty().addListener((obs, oldVal, newVal) ->
                slDealerTime.setValue(Math.round(newVal.doubleValue())));

        lbEngineStorageFullness.textProperty().bind(factory.getEngineStorageSizeProperty());
        lbCarcaseStorageFullness.textProperty().bind(factory.getCarcaseStorageSizeProperty());
        lbAccessoryStorageFullness.textProperty().bind(factory.getAccessoryStorageSizeProperty());
        lbCarStorageFullness.textProperty().bind(factory.getCarStorageSizeProperty());

        lbEngineTime.textProperty().bind(slEngineTime.valueProperty().asString());
        factory.getEngineAssemblyTimeProperty().bind(slEngineTime.valueProperty());
        lbCarcaseTime.textProperty().bind(slCarcaseTime.valueProperty().asString());
        factory.getCarcaseAssemblyTimeProperty().bind(slCarcaseTime.valueProperty());
        lbAccessoryTime.textProperty().bind(slAccessoryTime.valueProperty().asString());
        factory.getAccessoryAssemblyTimeProperty().bind(slAccessoryTime.valueProperty());
        lbDealerTime.textProperty().bind(slDealerTime.valueProperty().asString());
        factory.getDealerTimeProperty().bind(slDealerTime.valueProperty());
        dealerCars.textProperty().bind(factory.getDealedCars().asString());

    }

    public void stop() {
        factory.stop();
    }
}
